/// Quick hacky implementation of atomic f32.

use std::mem::transmute;
use std::sync::atomic::AtomicUsize;
pub use std::sync::atomic::Ordering;

fn to_usize(x: f32) -> usize {
    let x: u32 = unsafe{ transmute(x) };
    x as usize
}

fn from_usize(x: usize) -> f32 {
    let x = x as u32;
    unsafe{ transmute(x) }
}

pub struct AtomicF32(AtomicUsize);

impl AtomicF32 {
    pub const fn new_zero() -> AtomicF32 {
        AtomicF32(AtomicUsize::new(0))
    }

    #[allow(dead_code)]
    pub fn new(v: f32) -> AtomicF32 {
        AtomicF32(AtomicUsize::new(to_usize(v)))
    }

    pub fn load(&self, order: Ordering) -> f32 {
        from_usize(self.0.load(order))
    }

    pub fn store(&self, v: f32, order: Ordering) {
        self.0.store(to_usize(v), order)
    }
}