#![feature(const_fn)]

extern crate dolphin;
extern crate portaudio as pa;
extern crate pcap;
extern crate time;
extern crate passive_dns;

use std::f32::consts::PI;
use std::cell::Cell;

use dolphin::{ethernet, ipv4, IResult};
use passive_dns::PassiveDns as Dns;

mod atomic_f32;
use atomic_f32::*;

fn main() {
    static HERTZ_TIME: (AtomicF32, AtomicF32) = (AtomicF32::new_zero(), AtomicF32::new_zero());
    std::thread::spawn(|| run(&HERTZ_TIME).expect("Error in run"));

    let device = pcap::Device::list()
        .expect("Failed to list devices")
        .into_iter()
        .find(|x| x.name == "wls8")
        .expect("any device doesn't exist, are you not on linux maybe?");

    let mut cap = device.open().expect("Failed to open device");
    // Want to set directionaly, but current I rely on it not being directional for dns...
    // cap.direction(pcap::Direction::Out).expect("Failed to set direction");

    let mut dns = Dns::new();

    while let Ok(packet) = cap.next() {
        let name = dns.handle_packet(packet.data);

        let ether_packet = match ethernet::Packet::parse(packet.data) {
            IResult::Done(_, packet) => packet,
            _ => continue
        };

        let ipv4_packet = match ipv4::Packet::parse(ether_packet.payload) {
            IResult::Done(_, packet) => packet,
            _ => continue
        };

        let new_hertz = hash_ip(ipv4_packet.destination_address, 240.0, 750.0);

        println!("{} {} {:?} \t\t {:?}", ipv4_packet.source_address, ipv4_packet.destination_address, new_hertz, name);

        HERTZ_TIME.0.store(new_hertz, Ordering::Relaxed);
        HERTZ_TIME.1.store(time::precise_time_s() as f32, Ordering::Relaxed);
    }
}

fn hash_ip(ip: std::net::Ipv4Addr, lower: f32, upper: f32) -> f32 {
    // Large prime number
    const P: u64 = 32_416_190_071;
    // Arbitrary number
    const K: u64 = 454838;

    let ip_o = ip.octets();
    let ip_u64 = (ip_o[0] as u64) << 24 |
        (ip_o[1] as u64) << 16 |
        (ip_o[2] as u64) << 8 |
        ip_o[3] as u64;

    let v = (upper - lower) as u64;
    let hash = ((ip_u64 * K) % P) % v;

    hash as f32 + lower
}

fn run(&(ref hertz, ref last_time): &'static (AtomicF32, AtomicF32)) -> Result<(), pa::Error> {
    let pa = pa::PortAudio::new()?;

    let def_output = pa.default_output_device()?;
    let output_info = pa.device_info(def_output)?;
    println!("Default output device info: {:#?}", &output_info);

    let frames_per_second = 48000.0;
    let settings = pa.default_output_stream_settings(
        /* channels */ 1, frames_per_second, 0
    )?;

    let time = Cell::new(0.0);
    let last_hertz = Cell::new(512.0);

    let callback = move |pa::OutputStreamCallbackArgs::<f32> { buffer, .. }| {
        if last_time.load(Ordering::Relaxed) as f64 + 0.25 < time::precise_time_s() {
            // TODO: Repeat last value somehow
            for i in 0.. buffer.len() {
                buffer[i] = 0.0;
            }

            return pa::Continue
        }
        let hertz = hertz.load(Ordering::Relaxed);

        if last_hertz.get() != hertz {
            last_hertz.set(hertz);
            // TODO: Adjust time for a smooth transition.
        }

        let seconds_per_frame = 1.0 / frames_per_second as f32;

        for i in 0.. buffer.len() {
            let t = time.get() * 2.0 * PI * hertz;
            buffer[i] =  t.sin();
            time.set( time.get() + seconds_per_frame );
        }

        pa::Continue
    };

    let mut stream = pa.open_non_blocking_stream(
        settings,
        callback
    )?;

    println!("{:?}", stream.info());

    stream.start()?;

    loop {std::thread::sleep_ms(u32::max_value())}
}